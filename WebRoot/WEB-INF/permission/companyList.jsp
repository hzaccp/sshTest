<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ include file="/comm/tag.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
  		<base href='<%=basePath%>'>
  		<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
  		<link rel='stylesheet' type='text/css' href='css/public.css'>
  		<script type='text/javascript'>
  			window.onload=function(){
  			}
  		</script>
  </head>
  <body>
<a href='permission/toAddCompany.jhtml'>add</a>
  <br /><br />
<table  class='table' cellpadding='0' cellspacing='0'>
	<tr>
		<th><nobr>编号</nobr></th>
		<th><nobr>简称</nobr></th>
		<th><nobr>创建时间</nobr></th>
		<th><nobr>联系号码</nobr></th>
		<th><nobr>状态</nobr></th>
		<th><nobr>最后修改时间</nobr></th>
		<th><nobr>最后修改人</nobr></th>
		<th><nobr>Logo </nobr></th>
		<th><nobr>编码</nobr></th>
		<th><nobr>联系人</nobr></th>
		<th><nobr>密码</nobr></th>
		<th><nobr>创建人</nobr></th>
		<th><nobr>ID</nobr></th>
		<th><nobr>注册邮箱</nobr></th>
		<th><nobr>证件电子档</nobr></th>
		<th><nobr>注册地址</nobr></th>
		<th><nobr>描述</nobr></th>
		<th><nobr>名称</nobr></th>
		<th><nobr>删除时间</nobr></th>
		<th><nobr>公司简介</nobr></th>
		<th>操作</th>
	</tr>
<s:iterator value="companyList" id="info" status="sta">
<tr>
	<td><s:property value="#sta.index+1"/></td>
	<td><nobr>&nbsp;<s:property value="#info.simpleName"/></nobr></td>
	<td><nobr>&nbsp;<s:property value="#info.createTime"/></nobr></td>
	<td><nobr>&nbsp;<s:property value="#info.phone"/></nobr></td>
	<td><nobr>&nbsp;<s:property value="#info.status"/></nobr></td>
	<td><nobr>&nbsp;<s:property value="#info.lastUpdateTime"/></nobr></td>
	<td><nobr>&nbsp;<s:property value="#info.lastUpdateUser"/></nobr></td>
	<td><nobr>&nbsp;<s:property value="#info.logoUrl"/></nobr></td>
	<td><nobr>&nbsp;<s:property value="#info.number"/></nobr></td>
	<td><nobr>&nbsp;<s:property value="#info.contact"/></nobr></td>
	<td><nobr>&nbsp;<s:property value="#info.password"/></nobr></td>
	<td><nobr>&nbsp;<s:property value="#info.creator"/></nobr></td>
	<td><nobr>&nbsp;<s:property value="#info.id"/></nobr></td>
	<td><nobr>&nbsp;<s:property value="#info.email"/></nobr></td>
	<td><nobr>&nbsp;<s:property value="#info.papersUrl"/></nobr></td>
	<td><nobr>&nbsp;<s:property value="#info.address"/></nobr></td>
	<td><nobr>&nbsp;<s:property value="#info.description"/></nobr></td>
	<td><nobr>&nbsp;<s:property value="#info.name"/></nobr></td>
	<td><nobr>&nbsp;<s:property value="#info.deleteTime"/></nobr></td>
	<td><nobr>&nbsp;<s:property value="#info.signature"/></nobr></td>
	<td>
	<nobr>
		<a href='permission/companyDetail.jhtml?id=<s:property value="encode(#info.id)"/>'>查看</a>&nbsp;
		<a href='permission/toUpdateCompany.jhtml?id=<s:property value="encode(#info.id)"/>'>编辑</a>&nbsp;
		<a href='permission/deleteCompany.jhtml?id=<s:property value="encode(#info.id)"/>'>删除</a>
	</nobr>
	</td>
</tr>
</s:iterator>
</table>
<s:form theme="simple" action="companyList.jhtml" id="pageForm">
	<p:page formName="pageForm"></p:page>
</s:form>
  </body>
</html>
