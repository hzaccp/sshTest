<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ include file="/comm/tag.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
  		<base href='<%=basePath%>'>
  		<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
  		<link rel='stylesheet' type='text/css' href='css/public.css'>
  		<script type='text/javascript'>
  			window.onload=function(){
  			}
  		</script>
  </head>
  <body>
   		<a href='javascript:window.history.back()'>返回</a>
   		<s:form>
		<s:label label="简称" name="companyInfo.simpleName"></s:label>
		<s:label label="创建时间" name="companyInfo.createTime"></s:label>
		<s:label label="联系号码" name="companyInfo.phone"></s:label>
		<s:label label="状态" name="companyInfo.status"></s:label>
		<s:label label="最后修改时间" name="companyInfo.lastUpdateTime"></s:label>
		<s:label label="最后修改人" name="companyInfo.lastUpdateUser"></s:label>
		<s:label label="Logo " name="companyInfo.logoUrl"></s:label>
		<s:label label="编码" name="companyInfo.number"></s:label>
		<s:label label="联系人" name="companyInfo.contact"></s:label>
		<s:label label="密码" name="companyInfo.password"></s:label>
		<s:label label="创建人" name="companyInfo.creator"></s:label>
		<s:label label="ID" name="companyInfo.id"></s:label>
		<s:label label="注册邮箱" name="companyInfo.email"></s:label>
		<s:label label="证件电子档" name="companyInfo.papersUrl"></s:label>
		<s:label label="注册地址" name="companyInfo.address"></s:label>
		<s:label label="描述" name="companyInfo.description"></s:label>
		<s:label label="名称" name="companyInfo.name"></s:label>
		<s:label label="删除时间" name="companyInfo.deleteTime"></s:label>
		<s:label label="公司简介" name="companyInfo.signature"></s:label>
   		</s:form>
  </body>
</html>
