<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ include file="/comm/tag.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
  		<base href='<%=basePath%>'>
  		<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
  		<link rel='stylesheet' type='text/css' href='css/public.css'>
  		<script type='text/javascript'>
  			window.onload=function(){
  			}
  		</script>
  </head>
  <body>
   		<s:form action="addCompany" >
   			<s:textfield name="companyInfo.simpleName" label="简称"></s:textfield>
   			<s:textfield name="companyInfo.createTime" label="创建时间"></s:textfield>
   			<s:textfield name="companyInfo.phone" label="联系号码"></s:textfield>
   			<s:textfield name="companyInfo.status" label="状态"></s:textfield>
   			<s:textfield name="companyInfo.lastUpdateTime" label="最后修改时间"></s:textfield>
   			<s:textfield name="companyInfo.lastUpdateUser" label="最后修改人"></s:textfield>
   			<s:textfield name="companyInfo.logoUrl" label="Logo "></s:textfield>
   			<s:textfield name="companyInfo.number" label="编码"></s:textfield>
   			<s:textfield name="companyInfo.contact" label="联系人"></s:textfield>
   			<s:textfield name="companyInfo.password" label="密码"></s:textfield>
   			<s:textfield name="companyInfo.creator" label="创建人"></s:textfield>
   			<s:textfield name="companyInfo.email" label="注册邮箱"></s:textfield>
   			<s:textfield name="companyInfo.papersUrl" label="证件电子档"></s:textfield>
   			<s:textfield name="companyInfo.address" label="注册地址"></s:textfield>
   			<s:textfield name="companyInfo.description" label="描述"></s:textfield>
   			<s:textfield name="companyInfo.name" label="名称"></s:textfield>
   			<s:textfield name="companyInfo.deleteTime" label="删除时间"></s:textfield>
   			<s:textfield name="companyInfo.signature" label="公司简介"></s:textfield>
   			<s:submit />
   		</s:form>
  </body>
</html>
