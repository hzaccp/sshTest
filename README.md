最近找了份去年的SSH例子，做个简单的配置分析，记录些技术点方便以后使用。<br />
CSDN:<a href="http://blog.csdn.net/hzaccp3/article/details/11949781">http://blog.csdn.net/hzaccp3/article/details/11949781</a>
<br />
<br />

<strong>技术点：</strong><br />
&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;SSH整合、AOPLog及Log4j使用、声明式事务使用<br />
<strong>版本：</strong><br />
&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;Struts2：2.3.15.1<br />
&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;Spring：2.0<br />
&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;Hibernate：3.2.0.cr4<br />
<strong>jar包：</strong><br />
<img src="http://img.blog.csdn.net/20130923195027656?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvaHphY2NwMw==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast" alt="" /><br />
其中:<br />
&nbsp; &nbsp; &nbsp; &nbsp; hzaccp.jar 为工具包，包含部分基类、时间处理及字符串处理等，源码也一起打包。<br />
<strong>目录结构：</strong><br />
<img src="http://img.blog.csdn.net/20130923195921515?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvaHphY2NwMw==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast" alt="" /><br />
1：左边是src，使用三层结构，由于用的是struts2，所以注入都在applicationContext.xml中完成。biz中注入ICompanyDao，action中注入ICompanyService。<br />
2：右图是web目录，将所有jsp都放在WEB-INF目录下，不能直接访问到，只能通过struts2引用过来。<br />
<strong>配置:</strong><br />
<span style="font-size:18px;color:#009900;background-color: rgb(204, 204, 204);">web.xml</span><br />

<pre name="code" class="xml">&lt;!-- 检索Spring的配置，可以定义多个配置文件。这里有两个适配器，第一个指WEB-IN下的xml， 第二个检索classpath(一般在WEB-INF\classes)，第一个*为项目，第二个*为模块， 如例子，在myproject项目中，permission模块下就有一个。 --&gt;
&lt;context-param&gt;
	&lt;param-name&gt;contextConfigLocation&lt;/param-name&gt;
	&lt;param-value&gt;/WEB-INF/applicationContext.xml,classpath:com/hzaccp/*/*/applicationContext.xml&lt;/param-value&gt;
&lt;/context-param&gt;

&lt;!-- 用于初始化Spring容器的Listener --&gt;
&lt;listener&gt;
	&lt;listener-class&gt;org.springframework.web.context.ContextLoaderListener&lt;/listener-class&gt;
&lt;/listener&gt;

&lt;!-- Spring提供的字符过滤器 --&gt;
&lt;filter&gt;
	&lt;filter-name&gt;encodingFilter&lt;/filter-name&gt;
	&lt;filter-class&gt;org.springframework.web.filter.CharacterEncodingFilter&lt;/filter-class&gt;
	&lt;init-param&gt;
		&lt;param-name&gt;encoding&lt;/param-name&gt;
		&lt;param-value&gt;utf-8&lt;/param-value&gt;
	&lt;/init-param&gt;
&lt;/filter&gt;
&lt;filter-mapping&gt;
	&lt;filter-name&gt;encodingFilter&lt;/filter-name&gt;
	&lt;url-pattern&gt;*.jsp&lt;/url-pattern&gt;
&lt;/filter-mapping&gt;

&lt;!-- Hibernate懒加载处理，很少用到这个 --&gt;
&lt;filter&gt;
	&lt;filter-name&gt;lazyLoadingFilter&lt;/filter-name&gt;
	&lt;filter-class&gt;org.springframework.orm.hibernate3.support.OpenSessionInViewFilter&lt;/filter-class&gt;
&lt;/filter&gt;

&lt;!-- Struts的入口，定义Struts2的FilterDispathcer的Filter。 --&gt;
&lt;filter&gt;
	&lt;filter-name&gt;struts2&lt;/filter-name&gt;
	&lt;filter-class&gt;org.apache.struts2.dispatcher.ng.filter.StrutsPrepareAndExecuteFilter&lt;/filter-class&gt;
&lt;/filter&gt;
&lt;filter-mapping&gt;
	&lt;filter-name&gt;struts2&lt;/filter-name&gt;
	&lt;url-pattern&gt;/*&lt;/url-pattern&gt;
&lt;/filter-mapping&gt;

&lt;!-- 欢迎页 --&gt;
&lt;welcome-file-list&gt;
	&lt;welcome-file&gt;index.jsp&lt;/welcome-file&gt;
&lt;/welcome-file-list&gt;</pre>
<br />
<span style="font-size:18px;color:#009900;background-color: rgb(204, 204, 204);">src\struts.xml</span><br />

<pre name="code" class="xml">&lt;!-- struts的扩展名,以jhtml结尾。以前用.do、.action --&gt;
&lt;constant name=&quot;struts.action.extension&quot; value=&quot;jhtml&quot; /&gt;

&lt;!-- 是否使用spring注入 --&gt;
&lt;constant name=&quot;struts.objectFactory&quot; value=&quot;spring&quot; /&gt;

&lt;!-- 设置编码 --&gt;
&lt;constant name=&quot;struts.i18n.encoding&quot; value=&quot;utf-8&quot; /&gt;

&lt;!-- 是否使用动态方法拦截器 --&gt;
&lt;constant name=&quot;struts.enable.DynamicMethodInvocation&quot; value=&quot;false&quot; /&gt;

&lt;!-- 是否每次请求都去重新加载资源 --&gt;
&lt;constant name=&quot;struts.i18n.reload&quot; value=&quot;true&quot; /&gt;

&lt;!-- 开发模式，开启后，action会慢一个级别 --&gt;
&lt;constant name=&quot;struts.devMode&quot; value=&quot;true&quot; /&gt;

&lt;!-- 文件上传的临时目录。这个目录不一定会存在，上传完成后，文件也会自动删除 --&gt;
&lt;constant name=&quot;struts.multipart.saveDir&quot; value=&quot;c:/hzaccp/temp/&quot; /&gt;

&lt;!-- 文件上传大小，配置为10M，超过这个大小则报错 --&gt;
&lt;constant name=&quot;struts.multipart.maxSize&quot; value=&quot;10485760&quot; /&gt;

&lt;!-- 默认包 --&gt;
&lt;package name=&quot;default&quot; extends=&quot;struts-default&quot;&gt;
	&lt;action name=&quot;*&quot; class=&quot;com.hzaccp.comm.BaseAction&quot;&gt;
		&lt;result&gt;/{1}.jsp&lt;/result&gt;
	&lt;/action&gt;
&lt;/package&gt;

&lt;!-- 导入模块 --&gt;
&lt;include file=&quot;com/hzaccp/myporject/permission/action/Company-struts.xml&quot; /&gt;</pre>
<br />
<span style="font-size:18px;color:#009900;background-color: rgb(204, 204, 204);">src\struts.xml</span>
<pre name="code" class="xml">&lt;!-- 数据库账号和密码 --&gt;
&lt;property name=&quot;connection.username&quot;&gt;sa&lt;/property&gt;
&lt;property name=&quot;connection.password&quot;&gt;123456789&lt;/property&gt;

&lt;!-- 数据库连接信息,为本地的test数据库 --&gt;
&lt;property name=&quot;connection.url&quot;&gt;jdbc:sqlserver://127.0.0.1;databaseName=test&lt;/property&gt;

&lt;!-- 数据库方言，我用的是sqlServer，所以选用这个，也可以选用org.hibernate.dialect.MySQL5Dialect等。如果需要自定义hibernate函数，可以继承这个类，并指向自定义的类。 --&gt;
&lt;property name=&quot;dialect&quot;&gt;org.hibernate.dialect.SQLServerDialect&lt;/property&gt;

&lt;!-- sql驱动类 --&gt;
&lt;property name=&quot;connection.driver_class&quot;&gt;com.microsoft.sqlserver.jdbc.SQLServerDriver&lt;/property&gt;

&lt;!-- 输出sql --&gt;
&lt;property name=&quot;show_sql&quot;&gt;true&lt;/property&gt;

&lt;!-- 添加hibernate映射 --&gt;
&lt;mapping resource=&quot;com/hzaccp/myporject/permission/entity/CompanyInfo.hbm.xml&quot; /&gt;</pre>
<br />
<span style="font-size:18px;color:#009900;background-color: rgb(204, 204, 204);">WEB-INF\applicationContext.xml</span>
<pre name="code" class="xml">&lt;!-- 声明一个hibernate session工厂,并注入配置文件，也就是上面所说的hibernate.cfg.xml --&gt;
&lt;bean id=&quot;sessionFactory&quot; class=&quot;org.springframework.orm.hibernate3.LocalSessionFactoryBean&quot;&gt;
	&lt;property name=&quot;configLocation&quot; value=&quot;classpath:hibernate.cfg.xml&quot; /&gt;
&lt;/bean&gt;

&lt;!-- 声明hibernate事务管理器,并注入hibernate session工厂 --&gt;
&lt;bean id=&quot;transactionManager&quot; class=&quot;org.springframework.orm.hibernate3.HibernateTransactionManager&quot;&gt;
	&lt;property name=&quot;sessionFactory&quot;&gt;
		&lt;ref bean=&quot;sessionFactory&quot;&gt;&lt;/ref&gt;
	&lt;/property&gt;
&lt;/bean&gt;

&lt;!-- 使用声明式事务,对add、update、delete开头的方法使用事务，其他使用SUPPORTS事务。这里只是声明，并没有使用 --&gt;
&lt;tx:advice id=&quot;txAdvice&quot; transaction-manager=&quot;transactionManager&quot;&gt;
	&lt;tx:attributes&gt;
		&lt;tx:method name=&quot;add*&quot; propagation=&quot;REQUIRED&quot; /&gt;
		&lt;tx:method name=&quot;update*&quot; propagation=&quot;REQUIRED&quot; /&gt;
		&lt;tx:method name=&quot;delete*&quot; propagation=&quot;REQUIRED&quot; /&gt;
		&lt;tx:method name=&quot;*&quot; propagation=&quot;SUPPORTS&quot; read-only=&quot;true&quot; /&gt;
	&lt;/tx:attributes&gt;
&lt;/tx:advice&gt;

&lt;!-- 定义了一个AOP切入点,只对Biz层添加事务支持。使用前面配置的transactionManager是专对Hibernate的事务管理器 --&gt;
&lt;aop:config&gt;
	&lt;aop:pointcut id=&quot;allServiceMethods&quot; expression=&quot;execution(* com.hzaccp.*.*.biz.impl.*.*(..))&quot; /&gt;
	&lt;aop:advisor advice-ref=&quot;txAdvice&quot; pointcut-ref=&quot;allServiceMethods&quot; /&gt;
&lt;/aop:config&gt;

&lt;!-- 日志AOP,与事务类似 --&gt;
&lt;bean id=&quot;methodAdive&quot; class=&quot;com.hzaccp.comm.AOPLogBean&quot;&gt;&lt;/bean&gt;
&lt;aop:config&gt;
	&lt;aop:pointcut id=&quot;allMethod&quot; expression=&quot;execution(* com.hzaccp.*.*.biz.impl.*.*(..))&quot; /&gt;
	&lt;aop:aspect id=&quot;logAspect&quot; ref=&quot;methodAdive&quot;&gt;
		&lt;aop:around method=&quot;intercept&quot; pointcut-ref=&quot;allMethod&quot; /&gt;
	&lt;/aop:aspect&gt;
&lt;/aop:config&gt;

&lt;!-- 声明DAO父类，并注入Hibernate的session工厂 --&gt;
&lt;bean id=&quot;baseDAO&quot; class=&quot;com.hzaccp.comm.dao.HibernateBaseDAO&quot;&gt;
	&lt;property name=&quot;sessionFactory&quot; ref=&quot;sessionFactory&quot; /&gt;
&lt;/bean&gt;</pre>
<br />
<span style="font-size:18px;color:#009900;background-color: rgb(204, 204, 204);">src\log4j.properties</span><br />

<pre name="code" class="plain">#DEBUG级别,并声明两个记录器
log4j.rootLogger=DEBUG,stdout,hzaccp

#stdout记录器输出到控制台
log4j.appender.stdout=org.apache.log4j.ConsoleAppender
log4j.appender.stdout.layout=org.apache.log4j.PatternLayout
log4j.appender.stdout.layout.ConversionPattern=%d %5p (%c:%L) - %m%n

#hzaccp记录器输出到文件
log4j.appender.hzaccp=org.apache.log4j.RollingFileAppender
log4j.appender.hzaccp.File=hzaccp.log  
log4j.appender.hzaccp.MaxFileSize=100KB
log4j.appender.hzaccp.MaxBackupIndex=1
log4j.appender.hzaccp.layout=org.apache.log4j.PatternLayout
log4j.appender.hzaccp.layout.ConversionPattern=%-d{yyyy-MM-dd HH:mm:ss} [%c]-[%p] %m -(:%L)%n

#以下添加各个模块的输出过滤
# Print only messages of level FATAL or above in the package noModule.
log4j.logger.noModule=FATAL

# Freemarker模块
# struts2升级到2.3.15.1后，jsp界面用到struts标签后，输出的log
log4j.logger.freemarker=ERROR
log4j.logger.freemarker.cache=INFO

# OpenSymphony模块
log4j.logger.com.opensymphony=INFO
log4j.logger.org.apache.struts2=INFO

# Hibernate模块
log4j.logger.org.hibernate=INFO

# Spring模块
log4j.logger.org.springframework=INFO

# hzaccp模块
log4j.logger.com.hzaccp=DEBUG</pre>
<br />
<br />
模块内部醒置:每个模块都会包含action、biz、dao及entity包。在action中，包含各个组件的action文件及对应struts.xml文件。entity包中包含组件的Info及对应的hbm.xml文件。<br />
<strong>具体配置:</strong><br />
<span style="font-size:18px;color:#009900;background-color: rgb(204, 204, 204);">com.hzaccp.myporject.permission.applicationContext.xml</span><br />

<pre name="code" class="xml">&lt;!-- dao --&gt;
&lt;bean id=&quot;companyDAO&quot; class=&quot;com.hzaccp.myporject.permission.dao.impl.CompanyDAOImpl&quot; parent=&quot;baseDAO&quot; /&gt;

&lt;!-- biz --&gt;
&lt;bean id=&quot;companyService&quot; class=&quot;com.hzaccp.myporject.permission.biz.impl.CompanyServiceImpl&quot;&gt;
	&lt;property name=&quot;companyDAO&quot; ref=&quot;companyDAO&quot; /&gt;
&lt;/bean&gt;

&lt;!-- action --&gt;
&lt;bean id=&quot;companyAction&quot; class=&quot;com.hzaccp.myporject.permission.action.CompanyAction&quot; scope=&quot;prototype&quot;&gt;
	&lt;property name=&quot;companyService&quot; ref=&quot;companyService&quot; /&gt;
&lt;/bean&gt;</pre>
<br />
<span style="font-size:18px;color:#009900;background-color: rgb(204, 204, 204);">com.hzaccp.myporject.permission.action.Company-struts.xml</span><br />

<pre name="code" class="xml">&lt;!-- add --&gt;
&lt;action name=&quot;toAddCompany&quot; class=&quot;companyAction&quot; method=&quot;toAddCompany&quot;&gt;
	&lt;result name=&quot;success&quot;&gt;/WEB-INF/permission/addCompany.jsp&lt;/result&gt;
&lt;/action&gt;
&lt;action name=&quot;addCompany&quot; class=&quot;companyAction&quot; method=&quot;addCompany&quot;&gt;
	&lt;result name=&quot;success&quot; type=&quot;chain&quot;&gt;&lt;!-- 跳转至list --&gt;
		&lt;param name=&quot;actionName&quot;&gt;companyList&lt;/param&gt;
		&lt;param name=&quot;namespace&quot;&gt;/permission&lt;/param&gt;
	&lt;/result&gt;
&lt;/action&gt;

&lt;!-- update --&gt;
&lt;action name=&quot;toUpdateCompany&quot; class=&quot;companyAction&quot; method=&quot;toUpdateCompany&quot;&gt;
	&lt;result name=&quot;success&quot;&gt;/WEB-INF/permission/updateCompany.jsp&lt;/result&gt;
&lt;/action&gt;
&lt;action name=&quot;updateCompany&quot; class=&quot;companyAction&quot; method=&quot;updateCompany&quot;&gt;
	&lt;result name=&quot;success&quot; type=&quot;chain&quot;&gt;&lt;!-- 跳转至list --&gt;
		&lt;param name=&quot;actionName&quot;&gt;companyList&lt;/param&gt;
		&lt;param name=&quot;namespace&quot;&gt;/permission&lt;/param&gt;
	&lt;/result&gt;
&lt;/action&gt;

&lt;!-- list --&gt;
&lt;action name=&quot;companyList&quot; class=&quot;companyAction&quot; method=&quot;companyList&quot;&gt;
	&lt;result name=&quot;success&quot;&gt;/WEB-INF/permission/companyList.jsp&lt;/result&gt;
&lt;/action&gt;

&lt;!-- detail --&gt;
&lt;action name=&quot;companyDetail&quot; class=&quot;companyAction&quot; method=&quot;companyDetail&quot;&gt;
	&lt;result name=&quot;success&quot;&gt;/WEB-INF/permission/companyDetail.jsp&lt;/result&gt;
&lt;/action&gt;

&lt;!-- delete --&gt;
&lt;action name=&quot;deleteCompany&quot; class=&quot;companyAction&quot; method=&quot;deleteCompany&quot;&gt;
	&lt;result name=&quot;success&quot; type=&quot;chain&quot;&gt;&lt;!-- 跳转至list --&gt;
		&lt;param name=&quot;actionName&quot;&gt;companyList&lt;/param&gt;
		&lt;param name=&quot;namespace&quot;&gt;/permission&lt;/param&gt;
	&lt;/result&gt;
&lt;/action&gt;</pre>
各个action之间的跳转如图：<br />
<img src="http://img.blog.csdn.net/20130923210853859?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvaHphY2NwMw==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast" alt="" /><br />
<br />
<strong>总结：</strong><br />
1: web.xml中的contextConfigLocation,如果配置不正确，则无法检索到spring配置.<br />
2:WEB-INF\applicationContext.xml中的allServiceMethods及txAdvice，配置不正确会导致数据库死锁。如：在biz中其中一个方法执行了更新数据库，但该方法没在txAdvice配置，则事务就无法提交。<br />
3: struts.xml中的struts.devMode,该值为true时，Requests per second=573。为false时, Requests per second=1893。struts本来就慢，在部署后，必须关闭开发模式，有必要是，把AOP日志之类的也可以关闭。<br />
<br />