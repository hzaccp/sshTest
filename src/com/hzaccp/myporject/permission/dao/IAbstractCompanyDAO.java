package com.hzaccp.myporject.permission.dao;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.hzaccp.model.PagingBean;
import com.hzaccp.model.abstraction.QueryResult;
import com.hzaccp.myporject.permission.entity.CompanyInfo;
/**
*公司抽象接口
*com.hzaccp.myporject.permission.dao.IAbstractCompanyDAO
*/
public interface IAbstractCompanyDAO{
	/**
	*查询所有
	*/
	List<CompanyInfo> findAll();
	/**
	*根据HQL查询
	*/
	QueryResult<CompanyInfo> findByHQL(String hql);
	/**
	*根据实例查询
	*/
	QueryResult<CompanyInfo> findByExample(CompanyInfo info);
	/**
	*根据属性名查询
	*/
	QueryResult<CompanyInfo> findByProperty(String propertyName,Object value);
	/**
	*根据ID查询
	*/
	CompanyInfo findById(Serializable id);
	/**
	*新增操作
	*/
	void add(CompanyInfo info);
	/**
	*更新操作
	*/
	void update(CompanyInfo info);
	/**
	*删除操作
	*/
	void delete(CompanyInfo info);
	/**
	*根据Id删除操作
	*/
	int deleteById(Serializable id);
	/**
	*分页查询总行数
	*/
	int getCount(String hql, Map<String,Object> map);
	/**
	*分页查询
	*/
	List<CompanyInfo> findPaging(String hql, PagingBean pagingBean, Map<String,Object> map);
	/**
	*执行更新
	*/
	int executeUpdate(String hql,Map<String,Object> map);
	/**
	*通过sql查询数据
	*/
	List<Object> selectBySql(String sql, Map<String, Object> map);
	/**
	*通过sql执行更新
	*/
	int executeUpdateBySQL(String sql, Map<String, Object> map);
}