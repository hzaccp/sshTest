package com.hzaccp.myporject.permission.dao.impl;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;

import com.hzaccp.comm.dao.HibernateBaseDAO;
import com.hzaccp.model.PagingBean;
import com.hzaccp.model.abstraction.QueryResult;
import com.hzaccp.myporject.permission.dao.IAbstractCompanyDAO;
import com.hzaccp.myporject.permission.entity.CompanyInfo;
/**
*公司抽象实现类
*com.hzaccp.myporject.permission.dao.impl.AbstractCompanyDAOImpl
*/
@SuppressWarnings("unchecked")
public abstract class AbstractCompanyDAOImpl extends HibernateBaseDAO implements IAbstractCompanyDAO{
	/**
	*查询所有
	*/
	public List<CompanyInfo> findAll(){
		return super.findAll(CompanyInfo.class);
	}
	/**
	*根据HQL查询
	*/
	public QueryResult<CompanyInfo> findByHQL(String hql){
		return super.findByHQL(hql);
	}
	/**
	*根据实例查询
	*/
	public QueryResult<CompanyInfo> findByExample(CompanyInfo info){
		return super.findByExample(info);
	}
	/**
	*根据属性名查询
	*/
	public QueryResult<CompanyInfo> findByProperty(String propertyName,Object value){
		return super.findByProperty(CompanyInfo.class, propertyName, value);
	}
	/**
	*根据ID查询
	*/
	public CompanyInfo findById(Serializable id){
		return (CompanyInfo)super.findById(CompanyInfo.class, id);
	}
	/**
	*新增操作
	*/
	public void add(CompanyInfo info){
		super.save(info);
	}
	/**
	*更新操作
	*/
	public void update(CompanyInfo info){
		super.update(info);
	}
	/**
	*删除操作
	*/
	public void delete(CompanyInfo info){
		super.delete(info);
	}
	/**
	*根据Id删除操作
	*/
	public int deleteById(Serializable id){
		return executeUpdate("delete from CompanyInfo o where o.id=?",id);
	}
	/**
	*分页查询总行数
	*/
	@SuppressWarnings("rawtypes")
	public int getCount(String hql, Map map){
		QueryResult rs = super.findByHQL(hql, map);
		List list=rs.getList();
		if (list.size() > 0) {
			return Integer.parseInt(list.get(0).toString());
		} else {
			return 0;
		}
		
	}
	/**
	*分页查询
	*/
	@SuppressWarnings("rawtypes")
	public List<CompanyInfo> findPaging(String hql,PagingBean pagingBean,Map map){
		return super.findPaging(hql, pagingBean,map);
	}
	/**
	*执行更新
	*/
	@SuppressWarnings("rawtypes")
	public int executeUpdate(String hql,Map map){
		return super.executeUpdate(hql, map);
	}
	/**
	*通过sql查询数据
	*/
	public List<Object> selectBySql(String sql, Map<String, Object> map){
		final Query q = super.getSession().createSQLQuery(sql);
		if (map != null) {
			for (String key : map.keySet()) {
				q.setParameter(key, map.get(key));
			}
		}
		return new QueryResult<Object>() {
			public List<Object> getList() {
				return q.list();
			}
			public Object getUniqueResult() {
				return q.uniqueResult();
			}
		}.getList();
		
	}
	/**
	*通过sql执行更新
	*/
	public int executeUpdateBySQL(String sql, Map<String, Object> map){
		final Query q = super.getSession().createSQLQuery(sql);
		if (map != null) {
			for (Object key : map.keySet()) {
				q.setParameter(key.toString(), map.get(key));
			}
		}
		return q.executeUpdate();
		
	}
}