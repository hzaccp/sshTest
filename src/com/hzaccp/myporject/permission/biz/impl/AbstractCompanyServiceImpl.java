package com.hzaccp.myporject.permission.biz.impl;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.hzaccp.idConstructor.GUuid;
import com.hzaccp.model.PagingBean;
import com.hzaccp.model.abstraction.QueryResult;
import com.hzaccp.myporject.permission.biz.IAbstractCompanyService;
import com.hzaccp.myporject.permission.dao.ICompanyDAO;
import com.hzaccp.myporject.permission.entity.CompanyInfo;
/**
*公司抽象实现类
*com.hzaccp.myporject.permission.biz.impl.AbstractCompanyServiceImpl
*/
public abstract class AbstractCompanyServiceImpl implements IAbstractCompanyService{
	/**
	*DAO对象注入
	*/
	private ICompanyDAO companyDAO;
	public ICompanyDAO getCompanyDAO(){
		return this.companyDAO;
	}
	public void setCompanyDAO(ICompanyDAO companyDAO){
		this.companyDAO=companyDAO;
	}
	/**
	*查询所有
	*/
	public List<CompanyInfo> findAll(){
		return companyDAO.findAll();
	}
	/**
	*根据实例查询
	*/
	public QueryResult<CompanyInfo> findByExample(CompanyInfo example){
		return companyDAO.findByExample(example);
	}
	/**
	*根据属性名查询
	*/
	public QueryResult<CompanyInfo> findByProperty(String propertyName,Object value){
		return companyDAO.findByProperty(propertyName, value);
	}
	/**
	*根据ID查询
	*/
	public CompanyInfo findById(Serializable id){
		return companyDAO.findById(id);
	}
	/**
	*新增操作
	*/
	public void add(CompanyInfo example){
		if(example.getId()==null ||example.getId().length()==0){
			try {
				example.setId(GUuid.create(example.getObjType()).toString());
			} catch (Exception err) {
				err.printStackTrace();
			}
		}
		if(example.getCreateTime()==null){
			example.setCreateTime(new Date());
		}
		if(example.getLastUpdateTime()==null){
			example.setLastUpdateTime(new Date());
		}
		companyDAO.add(example);
	}
	/**
	*更新操作
	*/
	public void update(CompanyInfo example){
		if(example.getLastUpdateTime()==null){
			example.setLastUpdateTime(new Date());
		}
		companyDAO.update(example);
	}
	/**
	*删除操作
	*/
	public void delete(CompanyInfo example){
		companyDAO.delete(example);
	}
	/**
	*根据Id删除操作
	*/
	public boolean deleteById(Serializable id){
		return companyDAO.deleteById(id)>0;
	}
	/**
	*分页查询总行数
	*/
	public int getCount(String hql, Map<String,Object> map){
		return companyDAO.getCount(hql, map);
	}
	/**
	*分页查询
	*/
	public List<CompanyInfo> findPaging(String hql, PagingBean pagingBean, Map<String,Object> map){
		return companyDAO.findPaging(hql, pagingBean, map);
	}
	/**
	*通过sql查询数据
	*/
	public List<Object> selectBySql(String sql, Map<String, Object> map){
		return companyDAO.selectBySql(sql,map);
	}
	/**
	*通过sql执行更新
	*/
	public int executeUpdateBySQL(String sql, Map<String, Object> map){
		return companyDAO.executeUpdateBySQL(sql,map);
	}
}