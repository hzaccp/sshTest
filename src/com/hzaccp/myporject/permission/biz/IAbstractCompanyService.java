package com.hzaccp.myporject.permission.biz;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.hzaccp.model.PagingBean;
import com.hzaccp.model.abstraction.QueryResult;
import com.hzaccp.myporject.permission.entity.CompanyInfo;
/**
*公司抽象接口
*com.hzaccp.myporject.permission.biz.IAbstractCompanyService
*/
public interface IAbstractCompanyService{
	/**
	*查询所有
	*/
	List<CompanyInfo> findAll();
	/**
	*根据实例查询
	*/
	QueryResult<CompanyInfo> findByExample(CompanyInfo example);
	/**
	*根据属性名查询
	*/
	QueryResult<CompanyInfo> findByProperty(String propertyName,Object value);
	/**
	*根据ID查询
	*/
	CompanyInfo findById(Serializable id);
	/**
	*新增操作
	*/
	void add(CompanyInfo example);
	/**
	*更新操作
	*/
	void update(CompanyInfo example);
	/**
	*删除操作
	*/
	void delete(CompanyInfo example);
	/**
	*根据Id删除操作
	*/
	boolean deleteById(Serializable id);
	/**
	*分页查询总行数
	*/
	int getCount(String hql, Map<String,Object> map);
	/**
	*分页查询
	*/
	List<CompanyInfo> findPaging(String hql, PagingBean pagingBean, Map<String,Object> map);
	/**
	*通过sql查询数据
	*/
	List<Object> selectBySql(String sql, Map<String, Object> map);
	/**
	*通过sql执行更新
	*/
	int executeUpdateBySQL(String sql, Map<String, Object> map);
}