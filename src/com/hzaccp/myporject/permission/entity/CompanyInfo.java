package com.hzaccp.myporject.permission.entity;

import java.util.Date;

import com.hzaccp.comm.BaseEntity;

/**
*公司
*com.hzaccp.myporject.permission.entity.CompanyInfo
*/
public class CompanyInfo extends BaseEntity {
	private static final long serialVersionUID = 6944841310226431146L;
	private String simpleName;
	private Date createTime;
	private String phone;
	private Integer status;
	private Date lastUpdateTime;
	private String lastUpdateUser;
	private String logoUrl;
	private String number;
	private String contact;
	private String password;
	private String creator;
	private String id;
	private String email;
	private String papersUrl;
	private String address;
	private String description;
	private String name;
	private Date deleteTime;
	private String signature;

	public CompanyInfo() {

	}

	public CompanyInfo(String id) {
		this.id = id;
	}

	/**
	*获取ObjType值,此值与objType.properties中对应
	*/
	public String getObjType() {
		return "2F2D4BA6";
	}

	/**
	*简称
	*/
	public String getSimpleName() {
		return this.simpleName;
	}

	public void setSimpleName(String simpleName) {
		this.simpleName = simpleName;
	}

	/**
	*创建时间
	*/
	public Date getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	*联系号码
	*/
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	*状态
	*/
	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	*最后修改时间
	*/
	public Date getLastUpdateTime() {
		return this.lastUpdateTime;
	}

	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	/**
	*最后修改人
	*/
	public String getLastUpdateUser() {
		return this.lastUpdateUser;
	}

	public void setLastUpdateUser(String lastUpdateUser) {
		this.lastUpdateUser = lastUpdateUser;
	}

	/**
	*Logo 
	*/
	public String getLogoUrl() {
		return this.logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	/**
	*编码
	*/
	public String getNumber() {
		return this.number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	/**
	*联系人
	*/
	public String getContact() {
		return this.contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	/**
	*密码
	*/
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	/**
	*创建人
	*/
	public String getCreator() {
		return this.creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	/**
	*ID
	*/
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/**
	*注册邮箱
	*/
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	/**
	*证件电子档
	*/
	public String getPapersUrl() {
		return this.papersUrl;
	}

	public void setPapersUrl(String papersUrl) {
		this.papersUrl = papersUrl;
	}

	/**
	*注册地址
	*/
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	/**
	*描述
	*/
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	*名称
	*/
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	*删除时间
	*/
	public Date getDeleteTime() {
		return this.deleteTime;
	}

	public void setDeleteTime(Date deleteTime) {
		this.deleteTime = deleteTime;
	}

	/**
	*公司简介
	*/
	public String getSignature() {
		return this.signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}
}