package com.hzaccp.myporject.permission.action;

import com.hzaccp.comm.BaseAction;
import com.hzaccp.myporject.permission.entity.CompanyInfo;
import com.hzaccp.myporject.permission.biz.ICompanyService;
import java.util.List;

/**
*
*/
public class CompanyAction extends BaseAction {
	private static final long serialVersionUID = 7724981370518734154L;
	private CompanyInfo companyInfo;
	private ICompanyService companyService;
	private String id;
	private List<CompanyInfo> companyList;

	/**
	*添加
	*/
	public String toAddCompany() {
		return SUCCESS;
	}

	public String addCompany() {
		companyService.add(companyInfo);
		return SUCCESS;
	}

	/**
	*修改
	*/
	public String toUpdateCompany() {
		companyInfo = companyService.findById(id);
		return SUCCESS;
	}

	public String updateCompany() {
		companyService.update(companyInfo);
		return SUCCESS;
	}

	/**
	*分页查询所有
	*/
	@SuppressWarnings("unchecked")
	public String companyList() {
		String hql = "select o from com.hzaccp.myporject.permission.entity.CompanyInfo o";
		companyList = findPaging(companyService, hql, null);
		return SUCCESS;
	}

	/**
	*查看详情
	*/
	public String companyDetail() {
		companyInfo = companyService.findById(id);
		return SUCCESS;
	}

	/**
	* 删除
	*/
	@SuppressWarnings("unused")
	public String deleteCompany() {
		boolean isDeleted = companyService.deleteById(id);
		return SUCCESS;
	}

	public CompanyInfo getCompanyInfo() {
		return this.companyInfo;
	}

	public void setCompanyInfo(CompanyInfo companyInfo) {
		this.companyInfo = companyInfo;
	}

	public ICompanyService getCompanyService() {
		return this.companyService;
	}

	public void setCompanyService(ICompanyService companyService) {
		this.companyService = companyService;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<CompanyInfo> getCompanyList() {
		return this.companyList;
	}

	public void setCompanyList(List<CompanyInfo> companyList) {
		this.companyList = companyList;
	}
}